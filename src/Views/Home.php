<?php $title = "Bienvenue sur KadoKadeo !"; ?>

<?php ob_start(); ?>
<div class="inColumns">
	<section class="homeLeftColumn"></section>
	<section class="homeRightColumn">
        <h1 class="center">Bienvenue</h1>
        <h2 style="margin:0;">Des jeux</h2>
        <p>Plus de 70 jeux exclusifs à débloquer !</p>
        <h2>Plein d'amis</h2>
        <p>... et d'adversaires dans une communauté de joueurs pleine de bonne humeur !</p>
        <p class="center btnHomepagePlay"><a href="/signin" title="S'identifier / s'inscrire">>> Jouer <<</a></p>
	</section>
</div>
<?php $mainContent = ob_get_clean(); ?>

<?php include('Layouts/UnloggedLayout.php'); ?>
